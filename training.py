import pyarrow as pa
import pyarrow.parquet as pq
import numpy as np
import requests, os
import argparse
import joblib
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler, LabelEncoder
import mlflow
from sklearn import metrics
from dkube.sdk import *
import joblib

inp_path = "/opt/dkube/in"
out_path = "/opt/dkube/out"

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--url", dest="url", default=None, type=str, help="setup URL")
    parser.add_argument("--fs", dest="fs", required=True, type=str, help="featureset")

    global FLAGS
    FLAGS, unparsed = parser.parse_known_args()
    dkubeURL = FLAGS.url
    fs = FLAGS.fs

    ########--- Read features from input FeatureSet ---########

    # Featureset API
    authToken = os.getenv("DKUBE_USER_ACCESS_TOKEN")
    # Get client handle
    api = DkubeApi(URL=dkubeURL, token=authToken)

    # Read features
    feature_df = api.read_featureset(name = fs)  # output: data
    feature_df = feature_df.dropna()
    ########--- Train ---########
    insurance_input = feature_df.drop(['Credit Default'],axis=1)
    insurance_target = feature_df['Credit Default']
    
    #stadardize data
    x_scaled = StandardScaler().fit_transform(insurance_input)
    x_train, x_test, y_train, y_test = train_test_split(x_scaled,
                                                    insurance_target,
                                                    test_size = 0.25,
                                                    random_state=1211)
    #fit linear model to the train set data
    linReg = LogisticRegression()
    linReg_model = linReg.fit(x_train, y_train)
    
    y_pred_train = linReg.predict(x_train)    # Predict on train data.
    y_pred = linReg.predict(x_test)   # Predict on test data.
    
    #######--- Calculating metrics ---############
    cm = metrics.confusion_matrix(y_test, y_pred)
    cr = metrics.classification_report(y_test, y_pred)
    acc = metrics.accuracy_score(y_test, y_pred)

    print('Confustion Matrix:', cm)  
    print('Classification Report:', cr)  
    print('Accuracy:', acc)

    ########--- Logging metrics into Dkube via mlflow ---############
    mlflow.log_metric("Confustion Matrix:", cm)
    mlflow.log_metric("Classification Report:", cr)
    mlflow.log_metric("Accuracy:", acc)
    # Exporting model
    filename = os.path.join(out_path, "model.joblib")
    joblib.dump(linReg_model, filename)
